/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localraproxyinstance.h>
#include <buildboxcommon_grpcclient.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

LocalRaProxyInstance::LocalRaProxyInstance(
    std::shared_ptr<FsLocalAssetStorage> asset_storage,
    const buildboxcommon::ConnectionOptions &asset_endpoint)
    : RaInstance(), d_assetStorage(asset_storage),
      d_remoteInstanceName(asset_endpoint.d_instanceName)
{
    std::shared_ptr<buildboxcommon::GrpcClient> grpcClient =
        std::make_shared<buildboxcommon::GrpcClient>();
    grpcClient->init(asset_endpoint);
    d_assetClient =
        std::make_unique<buildboxcommon::AssetClient>(std::move(grpcClient));
    d_assetClient->init();
}

grpc::Status LocalRaProxyInstance::FetchBlob(const FetchBlobRequest &request,
                                             FetchBlobResponse *response)
{
    FetchBlobRequest newRequest = request;
    newRequest.set_instance_name(d_remoteInstanceName);
    *response = d_assetClient->fetchBlob(newRequest);

    if (response->status().code() == grpc::StatusCode::OK) {
        // Push to local cache
        PushBlobRequest pushRequest;
        pushRequest.add_uris(response->uri());
        pushRequest.mutable_qualifiers()->CopyFrom(request.qualifiers());
        pushRequest.mutable_expire_at()->CopyFrom(response->expires_at());
        pushRequest.mutable_blob_digest()->CopyFrom(response->blob_digest());

        d_assetStorage->insert(pushRequest);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalRaProxyInstance::FetchDirectory(const FetchDirectoryRequest &request,
                                     FetchDirectoryResponse *response)
{
    FetchDirectoryRequest newRequest = request;
    newRequest.set_instance_name(d_remoteInstanceName);
    *response = d_assetClient->fetchDirectory(newRequest);

    if (response->status().code() == grpc::StatusCode::OK) {
        // Push to local cache
        PushDirectoryRequest pushRequest;
        pushRequest.add_uris(response->uri());
        pushRequest.mutable_qualifiers()->CopyFrom(request.qualifiers());
        pushRequest.mutable_expire_at()->CopyFrom(response->expires_at());
        pushRequest.mutable_root_directory_digest()->CopyFrom(
            response->root_directory_digest());

        d_assetStorage->insert(pushRequest);
    }

    return grpc::Status::OK;
}

grpc::Status LocalRaProxyInstance::PushBlob(const PushBlobRequest &request,
                                            PushBlobResponse *)
{
    PushBlobRequest newRequest = request;
    newRequest.set_instance_name(d_remoteInstanceName);
    d_assetClient->pushBlob(newRequest);

    return grpc::Status::OK;
}

grpc::Status
LocalRaProxyInstance::PushDirectory(const PushDirectoryRequest &request,
                                    PushDirectoryResponse *)
{
    PushDirectoryRequest newRequest = request;
    newRequest.set_instance_name(d_remoteInstanceName);
    d_assetClient->pushDirectory(newRequest);

    return grpc::Status::OK;
}
