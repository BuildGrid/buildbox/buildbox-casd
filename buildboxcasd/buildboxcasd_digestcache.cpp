/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_digestcache.h>

#include <buildboxcasd_expiringunorderedset.h>

namespace buildboxcasd {

const int DigestCache::s_defaultEntrySecondsToLive = 5 * 60;

DigestCache::DigestCache(const int entrySecondsToLive)
    : d_entrySecondsToLive(entrySecondsToLive),
      d_cleanupPeriodSeconds(2 * d_entrySecondsToLive),
      d_set(entrySecondsToLive, d_cleanupPeriodSeconds)
{
}

bool DigestCache::hasDigest(const buildboxcommon::Digest &digest)
{
    return d_set.contains(digest);
}

bool DigestCache::addDigest(const buildboxcommon::Digest &digest)
{
    return d_set.insert(digest);
}

} // namespace buildboxcasd
