/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_DIGESTCACHE_H
#define INCLUDED_BUILDBOXCASD_DIGESTCACHE_H

#include <buildboxcasd_expiringunorderedset.h>
#include <buildboxcommon_protos.h>

namespace buildboxcasd {

class DigestCache final {
  public:
    DigestCache(const int entrySecondsToLive = s_defaultEntrySecondsToLive);

    bool hasDigest(const buildboxcommon::Digest &digest);

    bool addDigest(const buildboxcommon::Digest &digest);

  private:
    // Time that a digest will remain cached after its initial addition:
    static const int s_defaultEntrySecondsToLive;
    const int d_entrySecondsToLive;

    // How often to run the cleanup that removes expired entries from the
    // set's internal storage:
    const int d_cleanupPeriodSeconds;

    SelfCleaningExpiringUnorderedSet<buildboxcommon::Digest> d_set;
};

} // namespace buildboxcasd
#endif
